# SMARTER v2

[![Artifact Hub](https://img.shields.io/endpoint?url=https://artifacthub.io/badge/repository/smarter)](https://artifacthub.io/packages/search?repo=smarter)

We are currently refactoring and updating SMARTER and placing into this repository.  Some of the goals for this new version:

- Simplify deployment
  - move to helm based workflow for customization and deployment
  - simplify cloud based deployment to minimal set of services
  - leverage 3rd party platform deployment options versus custom yocto builds
- Update versions of baseline software
- Incorporate updates to triton and other software that forms the examples

